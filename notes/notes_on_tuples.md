#Tuple Notes
1. Yes, tuples and lists are both ordered sequences of data
2. Tuples are immutable. They cannot be modified, unlike lists. They are faster, more effecient, and use less RAM.
3. Tuple assignment is creative variables using a tuple and assigning them to data. (x, y) = (100, 20) is the same as x = 100, y = 20.
4. We can use tuples to sort by values instead of keys. We swap the value and key in a new list and then sort by value instead of key.
5. It makes sense to me, although I am not sure that I would be able to do it on my own without help. It confuses me how the for loop has nothing below it. It makes me think when it iterates it doesnt do anything.
