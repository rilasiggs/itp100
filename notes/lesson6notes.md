Lesson 6 Notes
1. Sequencial, conditional, store and reuse.
2. It counts down from a given number (n) and only exits the loop after the iteration variable is > 0.
3. Iterate.
4. An infinite loop. The iteration varibale never changes.
5. Break
6. Continue. It does not exit the loop but it restarts it so the loop runs it self again as if the loop had reached its final line.
7. Indefinite. They run infinitly until the iteration statement is false or a break occurs.
8. For loops. They assign the iteration variable to a value in a list variable then run the block. It then sets the iteration variable to the next value in the list variable and runs the code again.
9. Patterns that have to do with how we construct loops. Comparing two individual values in a list.
        a)Largest number in a set
        b)Counting the number of values in a list
        c)Sum of all values in a list
        d)Average of a ll values
        e)Filtering in a loop
        f)Search using boolean values
