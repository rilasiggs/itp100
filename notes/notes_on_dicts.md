# Notes on Dictionaries
1. A collection is a variable that stores multiple pieces of information with one name for all of the information, but also individual titles for each piece of information.
2. A list stores the information in an order so that you can find the 1st or 8th thing in the list, but a dictionary stores information with a title so you can find it by calling that title. Lists are like a can of pringles, they are stacked with an order and each "chip" has a position. Dictionaries are like a messy backpack. "Chips" are found by their labl, not their position in a sequence.
3. Associative arrays.
4. 
5. Histograms are great examples of applications of dictionaries.
6. `
counts = dict()
names = ['csev', 'cwen', csev', 'zqian', 'cwen']
for name in names:
    if name not in counts:
        counts[name] = 1
    else :
        counts[name] = counts[name] + 1
print(counts)
`
7. dictionary.get(key, default) This checks for a key in the given dictionary. If the key exists, Its value is returned, If not, it is set to the default value (in most cases its 0).
8. Tuples.