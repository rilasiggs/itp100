# Regular Expressions
1. They search through large blocks of text to find specific data (A structured way to find information)  **1960s**
2. **Quick Guide**
>^           Matches the beginning of a line    
>$           Matches the end of the line    
>.           Matchess any character  
>\s          Matches whitespace  
>\S          Matches any non-whitespace character   
>\*          Repeats a character zero or more times  
>\*?         Repeats a character zero or more times (non-greedy)    
>\+           Repeats a character one or more times  
>+?          Repeats a character one or more times (non-greedy)  
>[aeiou]     Matches a single character in the listed set   
>[^XYZ]      Matches a single character not in the listed set   
>[a-z0-0]    The set of characters can include a range  
>(           Indicates where string extraction is to start  
>)           Indicates where string extraction is to end    
3. The text file and python program are both in **itp100/exercises/regex**
4. ([VimRegex.com](http://vimregex.com/)) ([LearnByExample](https://learnbyexample.gitbooks.io/vim-reference/content/Regular_Expressions.html))
