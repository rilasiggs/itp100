import re
hand = open('10_lines.txt')
linecounter = 0
for line in hand:
    linecounter += 1
    line = line.strip()
    if re.findall('\S+@\S+', line) is not []:
        item1 = re.findall('\S+@\S+', line)
        count1 = linecounter

    if re.findall('^8: .+', line) is not []:
        item2 = re.findall('^8: .+', line)
        count2 = linecounter

    if re.findall('finally', line) is not []:
        item3 = re.findall('finally', line)
        count3 = linecounter
print(f'number of lines: {linecounter}')
print(item1, f'line {count1}')
print(item2, f'line {count2}')
print(item3, f'line {count3}')
