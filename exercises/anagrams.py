def anagram(name):
    handle = open(name)
    pos = []
    words = []
    for line in handle:
        words.append(line[:-1])
    sen = words[:1]
    words = words[1:]
    for placeholder in sen:
        sen = placeholder
    quota = dict()
    for letter in sen:
        if letter == ' ':
            continue
        else:
            quota[letter] = quota.get(letter,0) + 1
    for word in words:
        conts = dict()
        for char in word:
            conts[char] = conts.get(char,0) + 1
        try:
            for val in conts.keys():
                if conts[val] <= quota[val]:
                    continue
                else:
                    break
            pos.append(word)
        except:
            continue
    strg = []
    outpt = []
    quota3 = 0
    y = dict()
    for num in quota.values():
        quota3 += num
    for word in pos:
        y[word] = pos.copy()
        y[word].remove(word)
    strg = y.copy()
    while not strg == {}:
        y = strg
        strg = dict()
        for combin1,combin2 in y.items():
            for word in combin2:
                if len(combin1.replace(' ','')+word) >= quota3:
                    outpt.append(combin1 + ' ' + word)
                else:
                    strg[(combin1 + ' ' + word)] = combin2.copy()
                    strg[(combin1 + ' ' + word)].remove(word)
    strg = outpt.copy()
    outpt = []
    for atmpt in strg:
        if len(atmpt.replace(' ','')) == quota3:
            outpt.append(atmpt)
    strg = outpt.copy()
    for atmpt in strg:
        conts = dict()
        for char in atmpt.replace(' ',''):
            conts[char] = conts.get(char,0) + 1
        for term in conts.keys():
            if conts[term] == quota[term]:
                continue
            else:
                outpt.remove(atmpt)
                break
    strg = outpt.copy()
    outpt = []
    for angm in strg:
        count = len(angm.split())
        for word in angm.split():
            if word in pos:
                count += -1
            else:
                break
        if count == 0:
            for word in angm.split():
                pos.remove(word)
            outpt.append(angm)
    if outpt == []:
        print('No anagrams in this sentence')
    else:
        for angm in outpt:
            print(angm)


