def count_sub(sub, s):
    """
      >>> count_sub('is', 'Mississippi')
      2
      >>> count_sub('an', 'banana')
      2
      >>> count_sub('ana', 'banana')
      1
      >>> count_sub('nana', 'banana')
      1
      >>> count_sub('nanan', 'banana')
      0
    """
    for i in s:
        numberofsubs = s.count(sub)
        return numberofsubs

if __name__ == '__main__':
    import doctest
    doctest.testmod()
