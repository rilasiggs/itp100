def num_digits(n):
    """
      >>> num_digits(12345)
      5
      >>> num_digits(0)
      1
      >>> num_digits(-12345)
      5
    """
    n = str(n)
    count = 0
    for i in n:
        if i in "0123456789":
            count += 1
    return count


if __name__ == '__main__':
    import doctest
    doctest.testmod()
