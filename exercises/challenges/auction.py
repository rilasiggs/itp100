def revenue(bids):
    """
      >>> revenue('3 1 1 4 2')
      '4'
      >>> revenue('3 3 1 2 3 2 3 1 3 1 2')
      '9'
    """
    bids = bids.split() 
    num_agents = bids[1]
    num_items = bids[2]
    bids = bids[2:]
    item1 = bids[:3]
    item2 = bids[3:6]
    item3 = bids[6:9]
    highest1 = max(item1)
    highest2 = max(item2)
    highest3 = max(item3)
    profit = highest1 + highest2 + highest3
    return(item1)

if __name__ == '__main__':
    import doctest
    doctest.testmod()
