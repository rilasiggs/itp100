#import statements
from guizero import App, Text, TextBox, PushButton, Slider, Picture
JeffVis = 0

def displaytext():
    TextMes.value = TBox.value

def textslider(slider_value):
    TextMes.size = slider_value

def peekaboo():
    global JeffVis
    if JeffVis == 0:
        JeffPic.visible = True
        MyGui.height = 630
        JeffVis = 1
    else:
        JeffPic.visible = False
        MyGui.height = 230
        JeffVis = 0

# this creates the gui app and assigns it to the variable named MyGui
MyGui = App(title="This is The Title of My GUI", width=500, height=230, bg ="cyan")

# Creating text under he variale TextMes. It applies to the MyGui app and says "this is a sample text"
TextMes = Text(MyGui, "This is a sample text", size=50, font="Times New Roman", color="darkred")

# Adds a textbox for typing in
TBox = TextBox(MyGui)

# Adds a button that calls the function "displaytext" when pressed
Button = PushButton(MyGui, command=displaytext,text="Apply Text")

# creates a slider with numbers between 10 and 80
SizeSlider = Slider(MyGui, command=textslider, start=10, end=50)

# Push the button to make Jeff Apear
JeffButton = PushButton(MyGui, text="Jeff?", command=peekaboo)

#loads a picture of Jeff into the gui
JeffPic = Picture(MyGui, image="images/Jeff.gif", align="top", visible=False)



# Display the app titled MyGui
MyGui.display()
