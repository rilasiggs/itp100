from guizero import App, Text, TextBox, PushButton, Slider

def print_text():
    welcome_message.value = my_name.value

def change_text_size(slider_value):
    welcome_message.size = slider_value

app = App(title="Hello world")
welcome_message = Text(app, text = "Welcome to my app", font="Times New Roman", color="darkred")
my_name = TextBox(app)
update_text = PushButton(app, command=print_text, text="Display Text")
text_size = Slider(app, command=change_text_size, start=10, end=80)
app.display()
