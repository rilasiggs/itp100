#import statements
from guizero import App, Text, PushButton, Picture, ButtonGroup

def peekaboo():
    var = ButtonG.value
    if var == "Jeff":
        JeffPic.visible = True
        ChuckPic.visible = False
        MyGui.height = 630
    elif var == "Dr. Chuck":
        ChuckPic.visible = True
        JeffPic.visible = False
        MyGui.height = 630
    else:
        ChuckPic.visible = False
        JeffPic.visible = False
        MyGui.height = 230

MyGui = App(title="Jeff or Chuck, Quite The Decision", width=500, height=230, bg ="cyan")
TextMes = Text(MyGui, "Who would you like to see?", size=40, font="Times New Roman", color="darkred")
ButtonG = ButtonGroup(MyGui, options=["None", "Jeff", "Dr. Chuck"])
PicButton = PushButton(MyGui, text="Display", command=peekaboo)

JeffPic = Picture(MyGui, image="images/Jeff.gif", visible=False)
ChuckPic = Picture(MyGui, image="images/DrChuck.gif", visible=False)


MyGui.display()
